package de.classes.java.toaster;

public class Großer_Toaster extends Toaster{
	
	private final int g_schaechte = 4;
	private final String g_farbe = "blau";
	
	public Großer_Toaster(int k_anzahl_toast, int k_zeit) {	
//Super Klasse mit Eingaben vom Scanner aufrufen
		super(k_anzahl_toast, k_zeit);
		
//Die Variablen von der Super Klasse füllen
		schaechte = g_schaechte;
		farbe = g_farbe; 
		
//einwerfen() Methode aufrufen um den Toast Vorgang zu starten
		super.Toast_einwerfen();
	}

}
