package de.classes.java.toaster;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//Eingabefelder bauen für Toastertyp, Anzahl der Toasts und Zeitspanne		
		Scanner scan = new Scanner(System.in); 
		System.out.print("Kleiner oder großer oder super Toaster: ");
		String auswahl = scan.next();


//Auswahl welcher Toaster benutzt wird und je nach Toaster eingabefelder hinzufügen
		
		if(auswahl.equals("groß") || auswahl.equals("Groß")){
//Eingabefelder für den großen Toaster			
			System.out.print("Ihr Toast Anzahl(Max. 4): ");
			int anzahl = scan.nextInt();
			
			System.out.print("Wie lange soll es toasten(Max. 300sec): ");
			int zeit = scan.nextInt();
			//Großen Toaster Objekt erzeugen und Parameter übergeben			
			Großer_Toaster GroßerToaster = new Großer_Toaster(anzahl, zeit);
			
		}else if(auswahl.equals("klein") || auswahl.equals("Klein")) {
//Eingabefelder für den kleinen Toaster			
			System.out.print("Ihr Toast Anzahl(Max. 2): ");
			int anzahl = scan.nextInt();
			
			System.out.print("Wie lange soll es toasten(Max. 300sec): ");
			int zeit = scan.nextInt();
			//Kleinen Toaster Objekt erzeugen und Parameter übergeben			
			Kleiner_Toaster KleinerToaster = new Kleiner_Toaster(anzahl, zeit);
			
		}else if(auswahl.equals("super") || auswahl.equals("Super")){	
//Eingabefelder für den super Toaster			
			System.out.print("Ihr Toast Anzahl(Max. 6): ");
			int anzahl = scan.nextInt();
			
			System.out.print("Wie lange soll es toasten(Max. 300sec): ");
			int zeit = scan.nextInt();
			
			System.out.print("Gradzahl: ");
			int grad = scan.nextInt();
			//Super Toaster Objekt erzeugen und Parameter übergeben
			Super_Toaster SuperToaster = new Super_Toaster(anzahl, zeit, grad);	
			
		}else {
			System.out.println("Ungültige Eingabe! Bitte nur klein, groß oder super auswählen.");
		}		
		
		scan.close();
	}

}
