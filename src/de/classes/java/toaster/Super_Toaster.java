package de.classes.java.toaster;

public class Super_Toaster extends Toaster{
	
	private final int k_schaechte = 6;
	private final String k_farbe = "orange";
	
	private int grad;
	
	public Super_Toaster(int k_anzahl_toast, int k_zeit, int k_grad) {	
		//Super Klasse mit Eingaben vom Scanner aufrufen
		super(k_anzahl_toast, k_zeit);
		
		grad = k_grad;
//Die Variablen von der Super Klasse füllen
		schaechte = k_schaechte;
		farbe = k_farbe;
		
//Gradzahl prüfen und einwerfen() Methode aufrufen um den Toast Vorgang zu starten
		if(grad >= 500) {
			System.out.println("Viel zu heiß!! Bitte Temperatur runter regulieren.");
		}else {
			super.Toast_einwerfen();
		}
	}

}
