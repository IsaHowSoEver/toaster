package de.classes.java.toaster;

public class Kleiner_Toaster extends Toaster{
	
	private final int k_schaechte = 2;
	private final String k_farbe = "weinrot";
	
	public Kleiner_Toaster(int k_anzahl_toast, int k_zeit) {	
//Super Klasse mit Eingaben vom Scanner aufrufen
		super(k_anzahl_toast, k_zeit);
		
//Die Variablen von der Super Klasse füllen
		schaechte = k_schaechte;
		farbe = k_farbe;
		
//einwerfen() Methode aufrufen um den Toast Vorgang zu starten
		super.Toast_einwerfen();
	}

}
