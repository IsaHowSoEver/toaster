package de.classes.java.toaster;

public class Toaster {
	protected int schaechte;
	protected String farbe;
	
	protected int anzahl_toast;	
	protected int zeit;	
	protected String zustand;	

	private String ausgabetext;
	
//zuweisung der übergebenen Werte	
	public Toaster(int k_anzahl_toast, int k_zeit) {
		anzahl_toast = k_anzahl_toast;
		zeit = k_zeit;
	}
	
	public void Toast_einwerfen() {
		if(anzahl_toast > schaechte) {
			ausgabetext = "Zu viel Toast. Nicht genug Platz im Toaster!";
			System.out.println(ausgabetext);
		}else{
			Zeit_einstellen(zeit);
		}
	}
	
	private void Zeit_einstellen(int z_zeit) {
		if(z_zeit > 300) {
			ausgabetext = "Wenn sie Kohle essen wollen bitte. Ansonsten ist die Dauer viel zu lang. Bitte reduizieren!";
			System.out.println(ausgabetext);
		}else if(z_zeit <= 150 && z_zeit > 50) {
			zustand = "leicht getoastet";
			toasten();
		}else if(z_zeit <= 300 && z_zeit > 50) {
			zustand = "stark getoastet";
			toasten();
		}else if(z_zeit < 50) {
			ausgabetext = "Ihr Toast muss mindestens 50sec. getoastet werden!";
			System.out.println(ausgabetext);
		}
	}
	
	private void toasten() {
		System.out.println("Toast toastet gerade");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Toast_auswerfen(anzahl_toast);
	}
	private void Toast_auswerfen(int a_anzahl_toast) {
		ausgabetext = 
				"Ihr Toast ist "+zustand+"."+
				a_anzahl_toast+
				" Toasts aus ihrem "+farbe+
				" farbenen Toaster ausgeworfen, Toaster ist leer!";
		System.out.println(ausgabetext);
	}
}
